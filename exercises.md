# Exercises for 02148

## 1. Programming with spaces

### Exercise 1.1. Choosing a library
Go to [pSpace](https://github.com/pSpaces) and choose a library. We recommend to choose one of the following libraries:
* [jSpace](https://github.com/pSpaces/jSpace)
* [goSpace](https://github.com/pSpaces/goSpace)
* [dotSpace](https://github.com/pSpaces/dotSpace).

To choose the library take into account the programming language it is meant for (Java, C#, Go, Swift, JavaScript). Also, consider that the teaching assistants can provide support for [jSpace](https://github.com/pSpaces/jSpace), [goSpace](https://github.com/pSpaces/goSpace) and [dotSpace](https://github.com/pSpaces/dotSpace). If you choose one of the other libraries you may not get support from them or from the teacher.

### Exercise 1.2. Installing pSpaces
Install the library following the instructions in the corresponding repository.

### Exercise 1.3. Hello world!
Get the [hello world example](https://github.com/pSpaces/Programming-with-Spaces/blob/master/hello.md) to compile and run. 

### Exercise 1.4. Tutorial example
Get the example of the first chapter of the tutorial to compile and run:
* [example in Go](https://github.com/pSpaces/goSpace-examples/blob/master/tutorial/fridge-0/main.go) 
* [example in Java](https://gitlab.gbar.dtu.dk/02148/home/blob/master/solutions/java/Exercise_1_4.java)

If the  example is not provided in the programming language you have chosen, implement it yourself.

### Exercise 1.5. Playing with tuples
Play with the examples by trying the data types and operations seen in the tutorial.

## 2. Concurrent programming

### Exercise 2.1. Dining philosophers with a deadlock
The dining philosophers problem is a classical example of concurrent algorithms. A set of philosophers sit around a table. Each philosopher has a plate in front of him and there are forks in between each plate. A philosopher can grab the fork to the left of his plate and the one to the right of his fork. The lifecycle of a philosoper consist in continuously alternating thinking and lunch times. However, in order to eat they need to use two forks. Have a look at the dining philosophers code in this example:
* [example in Go](https://github.com/pSpaces/goSpace-examples/blob/master/tutorial/dining-philosophers-0/main.go)
* [example in Java](https://gitlab.gbar.dtu.dk/02148/home/blob/master/solutions/java/Exercise_2_1.java)

and get the example to compile and run. If the example is not provided in your language of choice, implement it yourself.

### Exercise 2.2. Deadlock-free dining philosophers
The code provided in the above example is not a suitable solution to the dining philosophers problem since the philosophers can end up in a deadlock, where no philosopher is able to progress. Explain why and modify the example to avoid deadlocks. HINT: there are many solutions to this problem that you can find on the web. You may also use one of the coordination patterns seen in the tutorial.

**SOLUTIONS**: 
 * Go: [solution 1](https://github.com/pSpaces/goSpace-examples/blob/master/tutorial/dining-philosophers-1/main.go)
 * Java: [solution 1](https://gitlab.gbar.dtu.dk/02148/home/blob/master/solutions/java/Exercise_2_3.java)

### Exercise 2.3. Parallel merge sort
A sorting algorithm takes a possibly disordered set of elements (e.g. a vector of integers) as input and returns the set of elements in order (e.g. as a vector of ordered integers). Sorting can be done in parallel, for example by parallelizing divide-and-conquer sorting algorithms like mergesort and quick-sort. Model and implement a parallel merge-sorting algorithm using tuple spaces. Use the bag of tasks coordination pattern where tasks are (ordered and unordered) vectors of integers and workers are specialised in various tasks: (i) splitting an unordered vector, (ii) merging two ordered vectors. 

**SOLUTIONS**:
 * Go: [solution 1](https://github.com/pSpaces/goSpace-examples/blob/master/tutorial/merge-sort-0/main.go)
 * Java: [solution 1](https://gitlab.gbar.dtu.dk/02148/home/blob/master/solutions/java/Exercise_2_3.java)

## 3. Semantics of pSpace programs
In the following exercises consider the [formal semantics for a light version of pSpaces](https://github.com/pSpaces/Programming-with-Spaces/blob/master/semantics-light.md) page and the transition systems obtained from such semantics: a transition system is a graph where nodes are programs and there is an edge from `M1 |- P1` to `M2 |- P2` if the semantics allows to conclude `M1 |-> P1 => M2 |-> P2`. 

### Exercise 3.1. Dining philosophers, formally
Consider the following light pSpace program `S` which represents a simple version of the dining philosophers problem:

```
board |-> Space("fork"*"fork") |- 
  space.get("fork").space.get("fork").space.put("fork").space.put("fork")
‖ space.get("fork").space.get("fork").space.put("fork").space.put("fork")
```

Construct the transition system reachable from `S`, i.e. start with the state/node `S` and keep adding transitions and new states until no more states/transitions can be added. If there are deadlock states (programs that cannot evolve) identify them.

**SOLUTION**: [solution](https://gitlab.gbar.dtu.dk/02148/home/blob/master/solutions/formal/exercise_3.1.md)

### Exercise 3.2. Deadlock-free dining philosophers, formally
Consider your solution to exercise 2.2. Update the program of exercise 3.1. to avoid deadlocks that are not finite states (i.e. of the form `M |- 0`).

**SOLUTION**: [solution 1](https://gitlab.gbar.dtu.dk/02148/home/blob/master/solutions/formal/exercise_3.2.md)


## 4. Modelling and analysing pSpaces programs with Spin

### Exercise 4.1. Installing SPIN
Install [SPIN](http://www.spinroot.com) or use the G-Bar installation of SPIN (accessible via [thinlinc](http://gbar.dtu.dk/faq/43-thinlinc)).

### Exercise 4.2. Playing with SPIN
Try all examples under "Examples" in the [notes on how to use the Spin model checker to model and verify pSpace applications](https://github.com/pSpaces/Programming-with-Spaces/blob/master/Promela/promela.md) page.

### Exercise 4.3. Checking deadlock-freedom of dining philosophers
Consider your solution to exercise 2.2. Does it correspond to some of the examples of Exercise 4.2? If not, try to model your solution in Promela and check whether it is deadlock free.

## 5. Distributed programming

### Exercise 5.1. Distributed dining philosophers
Consider your solution to exercise 2.2. Refactor your solution so that each tuple space is created and hosted by a different program and each philosopher runs as a standalone program. Try to run such distributed program in the same host and in different hosts.

**SOLUTIONS**:
 * Go: [solution 1](https://github.com/pSpaces/goSpace-examples/tree/master/tutorial/dining-philosophers-2)
 * Java [solution 1](https://gitlab.gbar.dtu.dk/02148/home/tree/master/solutions/java/Exercise_5_1)


### Exercise 5.2. Distributed merge sort
Consider your solution to exercise 2.3. Refactor your solution so that each tuple space is created and hosted by a different program and each worker runs as a standalone program. Try to run such distributed program in the same host and in different hosts.
 * Go: [solution 1](https://github.com/pSpaces/goSpace-examples/tree/master/tutorial/merge-sort-1)
 * Java [solution 1](https://gitlab.gbar.dtu.dk/02148/home/tree/master/solutions/java/Exercise_5_1)


## 6. Coordination models

TBD.

## 7. Secure spaces

TBD.