1. Programming with spaces
 * Preparation: get familiar with [pSpaces](https://github.com/pSpaces/) and read [tutorial 01](https://github.com/pSpaces/Programming-with-Spaces/blob/master/tutorial-tuple-spaces.md).
 * Content: [tutorial 01]( https://github.com/pSpaces/Programming-with-Spaces/blob/master/tutorial-tuple-spaces.md).
 * Exercises: Exercises 1.1-1.5 from the [exercise page](https://gitlab.gbar.dtu.dk/02148/home/blob/master/exercises.md).

2. Concurrent programming
 * Preparation: read [tutorial 02](https://github.com/pSpaces/Programming-with-Spaces/blob/master/tutorial-concurrent-programming.md).
 * Content: [tutorial 02](https://github.com/pSpaces/Programming-with-Spaces/blob/master/tutorial-concurrent-programming.md).
 * Exercises: Exercises: 2.1-2.3 from the [exercise page](https://gitlab.gbar.dtu.dk/02148/home/blob/master/exercises.md).
 
3. Semantics of pSpace programs
 * Preparation: read the [formal semantics for a light version of pSpaces](https://github.com/pSpaces/Programming-with-Spaces/blob/master/semantics-light.md)  page.
 * Content: the [formal semantics for a light version of pSpaces](https://github.com/pSpaces/Programming-with-Spaces/blob/master/semantics-light.md)  page.
 * Exercises: Exercises 3.1 and 3.2 in [exercise page](https://gitlab.gbar.dtu.dk/02148/home/blob/master/exercises.md).
 
4. Modelling and analysing pSpaces programs with Spin
 * Preparation: read about [how to use the Spin model checker to model and verify pSpace applications](https://github.com/pSpaces/Programming-with-Spaces/blob/master/Promela/promela.md).
 * Content: brief Spin tutorial and notes on [how to use the Spin model checker to model and verify pSpace applications](https://github.com/pSpaces/Programming-with-Spaces/blob/master/Promela/promela.md).
 * Exercises: Exercise 4.1-4.3 in [exercise page](https://gitlab.gbar.dtu.dk/02148/home/blob/master/exercises.md).

5. Distributed programming
 * Preparation: read [tutorial 03](https://github.com/pSpaces/Programming-with-Spaces/blob/master/tutorial-distributed-programming.md).
 * Content: [tutorial 03](https://github.com/pSpaces/Programming-with-Spaces/blob/master/tutorial-distributed-programming.md).
 * Exercises: Exercises 5.1 and 5.2 in [exercise page](https://gitlab.gbar.dtu.dk/02148/home/blob/master/exercises.md).

6. Coordination models
 * Preparation: -in preparation-
 * Content: -in preparation-
 * Exercises: -in preparation-
 
7. Secure Spaces
 * Preparation: -in preparation-
 * Content: -in preparation-
 * Exercises: -in preparation-
