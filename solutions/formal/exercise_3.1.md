```
board |-> Space("fork"*"fork") |- 
  space.get("fork").space.get("fork").space.put("fork").space.put("fork")
‖ space.get("fork").space.get("fork").space.put("fork").space.put("fork")

⇓

board |-> Space("fork") |-                                                      board |-> Space(nil) |- 
  space.get("fork").space.put("fork").space.put("fork")                   =>      space.get("fork").space.put("fork").space.put("fork")
‖ space.get("fork").space.get("fork").space.put("fork").space.put("fork")       ‖ space.get("fork").space.put("fork").space.put("fork")

⇓

board |-> Space(nil) |- 
  space.put("fork").space.put("fork")
‖ space.get("fork").space.get("fork").space.put("fork").space.put("fork")

⇓

board |-> Space("fork") |-                                                      board |-> Space(nil) |- 
  space.put("fork")                                                       =>      space.put("fork")  
‖ space.get("fork").space.get("fork").space.put("fork").space.put("fork")       ‖ space.get("fork").space.put("fork").space.put("fork")

⇓                                                                                    ||
                                                                                     ||
board |-> Space("fork"*"fork") |-                                                    ||
  space.get("fork").space.get("fork").space.put("fork").space.put("fork")            ||
                                                                                     ||
⇓                                                                                    ||  
                                                                                     ||
board |-> Space("fork") |-                                                       <==//
  space.get("fork").space.put("fork").space.put("fork")

⇓

board |-> Space(nil) |- 
  space.put("fork").space.put("fork")

⇓

board |-> Space("fork") |- 
  space.put("fork")

⇓

board |-> Space("fork"*"fork") |- 
  0


