```
board |-> Space("fork"*"fork"*"lock") |- 
  space.get("lock").space.get("fork").space.get("fork").space.put("fork").space.put("fork").put("lock")
‖ space.get("lock").space.get("fork").space.get("fork").space.put("fork").space.put("fork").put("lock")

⇓

board |-> Space("fork"*"fork") |- 
  space.get("fork").space.get("fork").space.put("fork").space.put("fork").put("lock")
‖ space.get("lock").space.get("fork").space.get("fork").space.put("fork").space.put("fork").put("lock")

⇓

board |-> Space("fork") |- 
  space.get("fork").space.put("fork").space.put("fork").put("lock")
‖ space.get("lock").space.get("fork").space.get("fork").space.put("fork").space.put("fork").put("lock")

⇓

board |-> Space(nil) |- 
  space.put("fork").space.put("fork").put("lock")
‖ space.get("lock").space.get("fork").space.get("fork").space.put("fork").space.put("fork").put("lock")

⇓

board |-> Space("fork") |- 
  space.put("fork").put("lock")
‖ space.get("lock").space.get("fork").space.get("fork").space.put("fork").space.put("fork").put("lock")

⇓

board |-> Space("fork"*"fork") |- 
  put("lock")
‖ space.get("lock").space.get("fork").space.get("fork").space.put("fork").space.put("fork").put("lock")

⇓

board |-> Space("fork"*"fork"*"lock") |- 
  space.get("lock").space.get("fork").space.get("fork").space.put("fork").space.put("fork").put("lock")
  
⇓

board |-> Space("fork"*"fork") |- 
  space.get("fork").space.get("fork").space.put("fork").space.put("fork").put("lock")

⇓

board |-> Space("fork") |- 
  space.get("fork").space.put("fork").space.put("fork").put("lock")

⇓

board |-> Space(nil) |- 
  space.put("fork").space.put("fork").put("lock")

⇓

board |-> Space("fork") |- 
  space.put("fork").put("lock")

⇓

board |-> Space("fork"*"fork") |- 
  put("lock")

⇓

board |-> Space("fork"*"fork"*"lock") |- 
  0


```

